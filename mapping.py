"""
Offline homography

Written by Constantin Ehrlich-Adám
pdz, ETH Zürich
2019
------------------------------------------------------------

Usage: run from the command line as such:
    python3 mapping.py --device=device-name --method=FLSD/SLSD --saveResults=bool --showMore=bool
"""

import cv2
import os

from Helper.mapping_helper import *

SCREEN_REF_COORDS = []
CORNER_REF_COORDS = []
TMP = []
IS_SCREEN_REF = bool

ROOT_DIR = './'

def click(event, x, y, flags, param):
    # grab references to the global variables
    global SCREEN_REF_COORDS, CORNER_REF_COORDS, IS_SCREEN_REF, TMP
    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        TMP.append((x,y))
        if IS_SCREEN_REF:
            cv2.circle(layout, (x,y), 10, (255, 0, 0), -1)
            cv2.imshow('GET REF COORDS', layout)
        else:
            cv2.circle(layout, (x,y), 10, (0, 0, 255), -1)
            cv2.imshow('GET REF COORDS', layout)

        if len(TMP) == 1:
            print('Top-Left: ', TMP[0])
        if len(TMP) == 2:
            print('Top-Right: ', TMP[1])
        if len(TMP) == 3:
            print('Bottom-Right: ', TMP[2])
        if len(TMP) == 4:
            print('Bottom-Left: ', TMP[3])
            print('Reference Coords:', TMP)

def get_ref_coords(layout):
    global TMP, SCREEN_REF_COORDS, CORNER_REF_COORDS, IS_SCREEN_REF
    TMP = []
    h,w = layout.shape[:2]
    cv2.namedWindow('GET REF COORDS')
    cv2.setMouseCallback('GET REF COORDS', click)
    cv2.putText(layout,'SCREEN_REF_COORDS',(50,50),cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,0))
    cv2.putText(layout,'CORNER_REF_COORDS',(w-500,50),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255))
    lc = layout.copy()

    # keep looping until the 'q' key is pressed
    while len(TMP) != 4:
        # display the image and wait for a keypress
        cv2.imshow('GET REF COORDS', layout)
        key = cv2.waitKey(1) & 0xFF
        # if the 'r' key is pressed, reset the cropping region
        if key == ord("r"):
            layout = lc.copy()
            print('Reset')
            TMP = []
            if IS_SCREEN_REF:
                SCREEN_REF_COORDS = []
                print('Screen Reference: ', SCREEN_REF_COORDS)
            else:
                CORNER_REF_COORDS = []
                print('Corner Reference: ', CORNER_REF_COORDS)

        # if the 'c' key is pressed, break from the loop
        elif (key == ord("c")):
            break

    if IS_SCREEN_REF:
        SCREEN_REF_COORDS = TMP[:]
    else:
        CORNER_REF_COORDS = TMP[:]
    print('SCREEN:', SCREEN_REF_COORDS)
    print('CORNER:', CORNER_REF_COORDS)

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Offline Homography')
    parser.add_argument("--device",
                        metavar="<device>",
                        help="device name")
    parser.add_argument("--method",
                        metavar="<method>",
                        help="method name SADAMET or FADAMET")
    parser.add_argument("--saveResults",
                        metavar="<saveResults>",
                        help="save results in a file")
    parser.add_argument("--showMore",
                        metavar="<showMore>",
                        help="Visualize what is happening")
    args = parser.parse_args()

    print('Device:', args.device)
    print('Method:', args.method)
    print('Show more:', args.showMore)
    print('Save Results: ', args.saveResults)
    showMore = True if args.showMore == 'True' else False
    saveResults = True if args.saveResults == 'True' else False


    print('Loading layout File')
    layout = get_layout(args.device, ROOT_DIR)
    color_copy = layout.copy()
    # Get the save coordinates from preparation
    SCREEN_REF_COORDS, CORNER_REF_COORDS = get_coords(ROOT_DIR, args.device)
    # Display the save coordinates
    for a,c in zip(SCREEN_REF_COORDS, CORNER_REF_COORDS):
        cv2.circle(color_copy, (a[0], a[1]),10,(255,255,0),-1)
        cv2.circle(color_copy, (c[0], c[1]),10,(0,255,255),-1)
        cv2.imshow('Reference coordinates for %s' %args.device, color_copy)
        cv2.waitKey(1)
    # If confirmation given proceed with homogrpahy otherwise get new reference coordinates
    print('Please confirm the coordinates (y/n)')
    inp = input()
    if inp == 'n':
        # Get Screen coords
        print('PLEASE SELECT THE SCREEN CORNERS IN CLOCKWISE DIRECTION')
        IS_SCREEN_REF = True
        get_ref_coords(layout)
        # Get Device coords
        print('PLEASE SELECT THE DEVICE CORNERS IN CLOCKWISE DIRECTION')
        IS_SCREEN_REF = False
        get_ref_coords(layout)

        print('COMPUTING THE HOMOGRAPYH MATRIX AND MAPPING THE GAZE POINT')
        offline_homography(layout, ROOT_DIR, args.device, args.method, CORNER_REF_COORDS, SCREEN_REF_COORDS, showMore, saveResults)
    elif inp =='y':
        print('COMPUTING THE HOMOGRAPYH MATRIX AND MAPPING THE GAZE POINT')
        offline_homography(layout, ROOT_DIR, args.device, args.method, CORNER_REF_COORDS, SCREEN_REF_COORDS, showMore, saveResults)

    print('DONE')