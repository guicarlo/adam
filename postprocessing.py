"""
AOI detection

Written by Constantin Ehrlich-Adám
pdz, ETH Zürich
2019
------------------------------------------------------------

Usage: run from the command line as such:
    # Apply AOI detection for given device
    python3 postprocessing.py --device=device_name --method=...
"""


from matplotlib import pyplot as plt

from Helper.postprocessing_helper import *
from Helper.visualize_helper import display_and_save

ROOT_DIR = './'

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Postprocessing.')
    parser.add_argument("--device", required=True,
                        metavar="<device>",
                        help="device name")
    parser.add_argument("--method", required=True,
                        metavar="<method>",
                        help="FLSD or SLSD")
    args = parser.parse_args()

    layout = get_layout(args.device, ROOT_DIR)

    aois, labels = get_aois(ROOT_DIR, args.device)

    aois_img = show_aoi(layout, aois)
    fileName = './Evaluation/postprocessing/%s_%s/aois.png' %(args.device, args.method)
    display_and_save('AOIs', aois_img, None, fileName, show=False, save=True, stack=False)

    pts, frame_nb = read_data(ROOT_DIR, args.device, args.method)
    timestamps = get_time(frame_nb, 10)
    pair, heat,focus, hits, order, res = show_points(ROOT_DIR, pts, frame_nb, aois, labels, layout, args.device, args.method)
    fileName = './Evaluation/postprocessing/%s_%s/points.png' %(args.device, args.method)
    display_and_save('Mapped Points', res, None, fileName, show=False, save=True, stack=False)
    dwell_times = get_dwell_times(hits)
    revisits = get_revisits(order, aois)
    info_img = show_aoi_info(dwell_times, revisits[0], aois, layout)
    fileName = './Evaluation/postprocessing/%s_%s/info.png' %(args.device, args.method)
    display_and_save('Info AOI', info_img, None, fileName, show=False, save=True, stack=False)

    figure = show_sequence_chart(pair,aois,labels,frame_nb)
    plt.savefig(os.path.join(ROOT_DIR,'Evaluation/postprocessing/%s_%s/sequence_chart.png' %(args.device, args.method)))

    heatmap, focusmap = show_heat_focus_map(heat,focus)
    fileName = './Evaluation/postprocessing/%s_%s/heat.png' %(args.device, args.method)
    heatmap.savefig(fileName)
    fileName = './Evaluation/postprocessing/%s_%s/focus.png' %(args.device, args.method)
    focusmap.savefig(fileName)




