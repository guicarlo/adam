import logging, coloredlogs

def init_logger(fileName):
    logger = logging.getLogger()

    coloredlogs.install(level=logging.INFO,
                        fmt='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%I:%S')
    # create file handler which logs even debug messages
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    fh = logging.FileHandler('./logs/LSD/%s.log' %fileName)
    fh.setFormatter(formatter)

    logger.addHandler(fh)