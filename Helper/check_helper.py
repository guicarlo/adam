import cv2
import numpy as np
from scipy.spatial import distance as dist
from math import *
from Helper.helper import get_ratios

def check_shape(pts_to_check, R):
    # Check if the shape of the point is within range -+20 %
    r1, r2 = get_ratios(pts_to_check)

    if 0.8*R < r1 < 1.2*R and 0.8*R < r2 < 1.2*R:
        return True, r1,r2
    else:
        return False, r1,r2

def check_intersection_location(intersections, pts_for_process, frame, EPSILON):
    # Check if the intersections are within the masked area
    # Create circle around old point with radius epsilon and check if within the circle
    radius = sqrt(2)*EPSILON
    fc = frame.copy()
    indices = []
    for k,(pt_int, pt_old) in enumerate(zip(intersections, pts_for_process)):
        center_x = pt_old[0]
        center_y = pt_old[1]
        x = pt_int[0]
        y = pt_int[1]
        cv2.circle(fc,(center_x, center_y), int(radius), (255,255,255), 5)
        is_inside = (x - center_x)**2 + (y - center_y)**2 <= radius**2
        if not is_inside:
            indices.append(k)
    return indices, fc

def check_for_outliers(xmvmts, ymvmts):
    # Check if any movement along x or y is an outlier using quartiles
    def outliers_iqr(ys):
        # get upper and lower quartile
        quartile_1, quartile_3 = np.percentile(ys, [25, 75])
        # get inter quartile range
        iqr = quartile_3 - quartile_1
        # extend range
        lower_bound = quartile_1 - (iqr * 1.5)
        upper_bound = quartile_3 + (iqr * 1.5)
        # check if within range
        idx = np.where((ys > upper_bound) | (ys < lower_bound))[0]
        return idx

    outliers = []
    outliers.append(outliers_iqr(xmvmts))
    outliers.append(outliers_iqr(ymvmts))
    idxx = None
    idxy = None
    # Check for outliers in x and y
    # If same outlier index return one index otherwise return two indices
    if len(outliers[0]) > 0:
        idxx = outliers[0][0]
    if len(outliers[1]) > 0:
        idxy = outliers[1][0]
    if idxx is not None and idxy is not None:
        return np.unique(np.array([idxx,idxy]))
    elif idxx is None and idxy is not None:
        return np.array([idxy])
    elif idxy is None and idxx is not None:
        return np.array([idxx])
    elif idxx is None and idxy is None:
        return np.array([])

def check_for_huge_mvmt(xmvmts,ymvmts):
    # Check if any point moved more than 50px
    return any(abs(mov) > 50 for mov in xmvmts) or any(abs(mov) > 50 for mov in ymvmts)

def check_all_sides_have_lines(lines_array):
    # Check if any side has no lines
    return all(len(l) > 0 for l in lines_array)

def check_error(err):
    # Check if any optical flow error is above threshold (8)
    return any(e[0] > 8 for e in err)

def monitor_mvmt(new_pts, SUB_ROI):
    # Get the movement of all the points compared to previous frame
    x_mvmts = []
    y_mvmts = []
    mvmts = []
    for ptN, ptS in zip(new_pts, SUB_ROI):
        oa = np.array(ptN)
        os = np.array(ptS)
        mov = oa-os
        mvmts.append(mov)
        x_mvmts.append(mov[0])
        y_mvmts.append(mov[1])
    return x_mvmts, y_mvmts, mvmts

