from datetime import datetime, timedelta
import numpy as np
import json
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import pandas
import os
import cv2
from math import *
import csv
import pdb

from Helper.visualize_helper import display_and_save
from Helper.helper import get_layout


def get_time(frame_nb, nb_of_ticks):
    times = []
    for t in frame_nb:
        tmp = t/24
        sec = timedelta(seconds=tmp)
        d = datetime(1,1,1) + sec
        times.append(('%s:%s,%s' %(d.minute,d.second,d.microsecond)))

    idx = np.round(np.linspace(0, len(times) - 1, nb_of_ticks)).astype(int)

    xticks = []
    for val in idx:
        xticks.append(times[val])

    return xticks

def get_aois(ROOT_DIR, device):
    aois_json = json.load(open(os.path.join(ROOT_DIR,'assets/%s/aois.json' %device)))
    aois = []
    aoi_names = []
    for aoi in aois_json:
        tmp = {
            'name': aoi['name'],
            'top-left': aoi['top-left'],
            'bottom-right': [aoi['top-left'][0]+aoi['w,h'][0],aoi['top-left'][1]+aoi['w,h'][1]],
        }
        aois.append(tmp)
        aoi_names.append(aoi['name'])

    return aois, aoi_names

def show_aoi(layout, aois):
    lc = layout.copy()
    for aoi in aois:
        cv2.rectangle(lc,(tuple(aoi['top-left'])),(tuple(aoi['bottom-right'])),(0,0,255),3)
        cv2.putText(lc, '%s' %aoi['name'],
                        (aoi['top-left'][0],aoi['top-left'][1]-10),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5,
                        (0,100,255))
    return lc

def read_data(ROOT_DIR, device_name, method):
    # Read file with mapped gaze points
    df = pandas.read_csv(os.path.join(ROOT_DIR,'text_files/eval_mapped_gaze_%s_%s.csv' %(device_name, method)))
    pts = []
    frame_nb = []
    for i,(nb, x,y) in enumerate(zip(df['frame'],df['x'], df['y'])):
        # if len(frame_nb) > 0:
            # if not any(x == nb for x in frame_nb):
        pt = (int(x),int(y))
        pts.append(pt)
        frame_nb.append(nb)
        # else:
        #     pt = (int(x),int(y))
        #     pts.append(pt)
        #     frame_nb.append(nb)
    return pts,frame_nb


def show_points(ROOT_DIR, pts, frame_nb, aois, labels, layout, device, method):
    res = layout.copy()
    heat = np.zeros((res.shape[0],res.shape[1]))
    focus = np.zeros((res.shape[0],res.shape[1]))
    heat[:,:] = 255
    focus[:,:] = 0
    hits = np.zeros((1,len(aois)),dtype='int32')
    pair = []
    order = []
    for l,(pt, fnb) in enumerate(zip(pts, frame_nb)):
        for idx, (aoi, aoi_name) in enumerate(zip(aois, labels)):
            if aoi['top-left'][0] < pt[0] < aoi['bottom-right'][0] and aoi['top-left'][1] < pt[1] < aoi['bottom-right'][1]:
                pair.append((l,idx))
                with open(os.path.join(ROOT_DIR,'text_files/aoi_frame_pair_%s_%s.csv' %(device,method)), mode='a') as eval_file:
                        eval_writer = csv.writer(eval_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        if l == 0:
                            eval_writer.writerow(['frame', 'AOI', 'AOI Name'])

                        eval_writer.writerow(['%s' %fnb, '%s' %idx, '%s' %aoi_name])

                hits[0][idx] += 1
                heat[pt[1]-30:pt[1]+30,pt[0]-30:pt[0]+30] -= 1
                focus[pt[1]-10:pt[1]+10,pt[0]-10:pt[0]+10] += 1
                order.append(idx)

                cv2.circle(res,(pt[0],pt[1]),10,(255,0,0), 5)
                cv2.rectangle(res,(tuple(aoi['top-left'])),(tuple(aoi['bottom-right'])),(0,150,255),3)

            cv2.rectangle(res,(tuple(aoi['top-left'])),(tuple(aoi['bottom-right'])),(0,0,0),3)

    return pair, heat, focus, hits, order, res

def show_heat_focus_map(heat, focus):
    fig1 = plt.figure()
    plt.imshow(heat, cmap='hot', interpolation='nearest')
    fig2 = plt.figure()
    plt.imshow(focus, cmap='winter', interpolation='nearest')
    return fig1, fig2


def get_dwell_times(hits):
    total= sum(hits[0])
    percentage = []
    for h in hits[0]:
        percentage.append(round(h/total*100,2))

    return percentage

def get_revisits(order, aois):
    revisits = np.zeros((1,len(aois)),dtype='int32')
    for i in range(0,len(order)-1,1):
        if order[i-1] != order[i]:
            revisits[0][order[i]] += 1

    return revisits

def show_aoi_info(dwell_times, revisits, aois, layout):
    lc = layout.copy()

    for d,rev,aoi in zip(dwell_times, revisits, aois):
        text_pos_x = int((aoi['bottom-right'][0] - aoi['top-left'][0])/4 + aoi['top-left'][0])
        text_pos_y = int((aoi['bottom-right'][1] - aoi['top-left'][1])/4 + aoi['top-left'][1])

        cv2.rectangle(lc,(tuple(aoi['top-left'])),(tuple(aoi['bottom-right'])),(0,0,255),3)
        cv2.putText(lc, '%s' %aoi['name'],
                        (text_pos_x,text_pos_y ),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5,
                        (15,0,155))
        cv2.putText(lc, 'Time: %s%%' %d,
                        (text_pos_x,text_pos_y + 15 ),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5,
                        (15,0,155))
        cv2.putText(lc, 'Revisits: %s' %rev,
                        (text_pos_x,text_pos_y + 30 ),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5,
                        (15,0,155))
    return lc


def show_sequence_chart(pair,aois,labels,frame_nb):
    # x = np.linspace(0, len(pair), 1)
    fig = plt.figure()
    plt.figure(figsize=(10,8))

    plt.yticks(np.arange(len(aois)),labels)
    plt.xticks(np.arange(len(frame_nb)),frame_nb)
    colors = cm.Paired(np.linspace(0, 1, len(labels)))
    for i, pt in enumerate(pair):
        plt.scatter(pt[0],pt[1], color=colors[pt[1]], marker='|')
    return fig