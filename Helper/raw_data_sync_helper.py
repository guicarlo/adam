import pandas
import os
import cv2
import datetime
import csv
from Helper.helper import printProgressBar

def read_raw_data(device, ROOT_DIR):
    df = pandas.read_csv(os.path.join(ROOT_DIR,'assets/Raw_Data/raw_data_%s.csv' %device), header=0,sep=';', low_memory=False)

    pts = []
    t_start = None
    for l,(cat,x,y,t) in enumerate(zip(df['Category Binocular'],
                    df['Point of Regard Binocular X [px]'],
                    df['Point of Regard Binocular Y [px]'],
                    df['Time of Day [h:m:s:ms]'])):
        if cat == 'Visual Intake':
            pt = {
                'time': None,
                'xy': None,
                'frame': None
            }
            try:
                pt['xy'] = (int(float(x)),int(float(y)))
                date_time_obj = datetime.datetime.strptime(t, '%H:%M:%S:%f')
                tot_sec = (date_time_obj-datetime.datetime(1900,1,1)).total_seconds()
                pt['time'] = tot_sec
                pts.append(pt)
            except:
                print('error in file', cat,x,y,t)
        if l == 0:# and cat != 'Visual Intake':
            date_time_obj = datetime.datetime.strptime(t, '%H:%M:%S:%f')
            t_start = (date_time_obj-datetime.datetime(1900,1,1)).total_seconds()

    for p in pts:
        p['time'] -= t_start

    return pts

def get_video_dimension(video_cap):
    VIDEO_HEIGHT = 4
    VIDEO_WIDTH = 3
    width = video_cap.get(VIDEO_WIDTH)
    height = video_cap.get(VIDEO_HEIGHT)
    return [width, height]

def get_nb_frames(video_cap):
    VIDEO_NB_FRAMES = 7
    nb_frames = video_cap.get(VIDEO_NB_FRAMES)
    return nb_frames

def get_frame_rate(video_cap):
    VIDEO_FRAME_RATE = 5
    frame_rate = video_cap.get(VIDEO_FRAME_RATE)
    return frame_rate

def read_video(device, ROOT_DIR):
    video = cv2.VideoCapture(os.path.join(ROOT_DIR,'assets/Videos/video_%s.avi' %device))
    dimension = get_video_dimension(video)
    nb_frames = get_nb_frames(video)
    frame_rate = get_frame_rate(video)


    print("Dimension: %sx%s" %(dimension[0], dimension[1]))
    print ("Number of frames: %s" %nb_frames)
    print ("Frame rate: %s" %frame_rate)

    return video, nb_frames

def build_time_ranges(nb_frames):
    time_range = [float(0), float(1/24)]
    times_ranges = []
    for i in range(0,int(nb_frames),1):
        times_ranges.append([float(0+i/24), float(0+(i+1)/24)])

    return times_ranges

def match_point_to_frame(pts,times_ranges):
    k = len(pts)
    printProgressBar(0, k, prefix = 'Progress:', suffix = 'Complete', length = 50)

    for i,p in enumerate(pts):
        for l,r in enumerate(times_ranges):
            if r[0] <= p['time'] < r[1]:
                p['frame'] = l
                break

        # Update Progress Bar
        printProgressBar(i + 1, k, prefix = 'Progress:', suffix = 'Complete', length = 50)

    last_frame = pts[-1]['frame']
    gaze = {}
    for i in range(pts[0]['frame'],pts[-1]['frame']+1):
        gaze[i] = []

    for pt in pts:
        gaze[pt['frame']].append(pt['xy'])

    return gaze

def write_to_file(device, gaze, ROOT_DIR):
    with open(os.path.join(ROOT_DIR,'text_files/gaze_pts_%s.csv' %device), 'a') as f:
        writer = csv.writer(f, delimiter=';',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for key in gaze.keys():
            f.write("%s;%s\n"%(key,gaze[key]))


def play_video_with_gaze(video,gaze):
    video.set(cv2.CAP_PROP_POS_FRAMES,28520)
    i = video.get(cv2.CAP_PROP_POS_FRAMES)
    gaze_counter = int(i-6)
    while gaze_counter < len(gaze)-1:
        print(gaze[gaze_counter])
        if len(gaze[gaze_counter]['pts'])>0:
            video.set(cv2.CAP_PROP_POS_FRAMES, gaze[gaze_counter]['frame'])

            i = video.get(cv2.CAP_PROP_POS_FRAMES)
            print(i,gaze_counter)
            ret, frame = video.read()

            if ret:
                fc = frame.copy()
                for a in  gaze[gaze_counter]['pts']:
                    cv2.circle(fc,(a[0], a[1]),20,(255,255,0), 12)
                    cv2.imshow('gaze',fc)
                    cv2.waitKey(1)
            gaze_counter += 1
        else: gaze_counter += 1