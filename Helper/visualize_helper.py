import cv2
import numpy as np

def draw_lines(lsd, lines, frame):
    frame_copy = frame.copy()
    for line in lines[0]:
        drawn_img = lsd.drawSegments(frame_copy,line)
    return drawn_img

def show_intersections(intersections, frame):
    fc = frame.copy()
    for pt in  intersections:
        cv2.circle(fc, pt, 10,(0,0,255),-1)
    return fc

def show_main_lines(main_lines, w_avg_lines, frame):
    fc = frame.copy()
    for ml, wal in zip(main_lines,w_avg_lines):
        cv2.line(fc,(int(ml[0]),int(ml[1])),(int(ml[2]),int(ml[3])),(255,100,0),3)
        cv2.line(fc,(int(wal[0]),int(wal[1])),(int(wal[2]),int(wal[3])),(0,255,255),3)
    return fc

def display_and_save(windowName, image, frame, fileName='', show=True, save=True, stack=True):
    # if show:
        # cv2.imshow(windowName,image)
        # cv2.waitKey(1)
    img_to_save = np.hstack((frame,image)) if stack else image
    if save:
        cv2.imwrite(fileName, img_to_save)

def show_seg_centers(center_pts, seg_by_angle_img):
    fc = seg_by_angle_img.copy()
    for l, sub in enumerate(center_pts):
        color = (0,255,255) if l == 0 else (255,255,0)
        for pt in sub:
            cv2.circle(fc, pt, 10,color,-1)
    return fc

def show_seg_by_angle(segmented_by_angle,frame):
    fc = frame.copy()
    for l, sub in enumerate(segmented_by_angle):
        color = (0,0,255) if l == 0 else (255,0,0)
        for line in sub:
            cv2.line(fc,(int(line[0][0]),int(line[0][1])),(int(line[0][2]),int(line[0][3])),color,3)
    return fc

def show_OF_mvmt(mask, frame, pts_for_process, frame_coords, color_OF):
    of_copy = frame.copy()
    for l,(new,old) in enumerate(zip(pts_for_process,frame_coords)):
        a,b = new.ravel()
        c,d = old.ravel()
        # mask = cv2.line(mask, (a,b),(c,d), color_OF[l].tolist(), 2)
        frame = cv2.circle(of_copy,(a,b),5,color_OF[l].tolist(),-1)
    img = cv2.add(of_copy,mask)
    cv2.imshow('Optical Flow movement',img)

def show_gaze_pts(gaze_pts, frame):
    fc = frame.copy()
    for pt in gaze_pts:
        cv2.circle(fc,(pt[0], pt[1]),20,(255,255,0), 12)
        cv2.imshow('Gaze Points', fc)

