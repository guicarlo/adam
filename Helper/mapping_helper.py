import json
import os
import numpy as np
import pandas
import cv2
import re
import os
import csv
import json

from Helper.helper import *
from Helper.lsd_helper import get_gaze_pts

def get_coords(ROOT_DIR, device):
    coords = json.load(open(os.path.join(ROOT_DIR, "assets/%s/layout/corner_coords.json" %device)))
    screen_ref = coords[0]['points']
    corner_ref = coords[1]['points']

    return screen_ref, corner_ref

def build_image(ref_coords):
    # Build the image canvas for the warped image
    y_coords = []
    x_coords = []

    for p in ref_coords:
        y_coords.append(p[1])
        x_coords.append(p[0])

    max_x_idx = np.argmax(x_coords)
    max_y_idx = np.argmax(y_coords)
    min_x_idx = np.argmin(x_coords)
    min_y_idx = np.argmin(y_coords)

    max_x = int(x_coords[max_x_idx])
    min_x = int(x_coords[min_x_idx])
    max_y = int(y_coords[max_y_idx])
    min_y = int(y_coords[min_y_idx])

    (tl, tr, br, bl) = ref_coords
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))

    # ...and now for the height of our new image
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))

    # take the maximum of the width and height values to reach
    # our final dimensions
    maxWidth = max(int(widthA), int(widthB))
    maxHeight = max(int(heightA), int(heightB))

    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype = "float32")

    return dst, maxWidth, maxHeight

def load_data(ROOT_DIR, device, method):
    df = pandas.read_csv(os.path.join(ROOT_DIR,'text_files/tracked_pts_%s_%s.csv' %(device,method)),sep=',',names=['idx', 'pts'],low_memory=False)
    frame_nb = []
    ordered_pts = []
    for i,val in zip(df['idx'],df['pts']):
        frame_nb.append(int(i))
        li = re.findall('\d+', val)
        ordered_pts.append([[int(li[0]),int(li[1])],[int(li[2]),int(li[3])],[int(li[4]),int(li[5])],[int(li[6]),int(li[7])]])

    return frame_nb, ordered_pts

def offline_homography(layout, ROOT_DIR, device, method, CORNER_REF_COORDS, SCREEN_REF_COORDS, showMore, saveResults):
    WARPED_SCREENS = []
    # open video
    cap = cv2.VideoCapture(os.path.join(ROOT_DIR,'assets/Videos/video_%s.avi' %device))

    gray_copy = cv2.cvtColor(layout, cv2.COLOR_RGB2GRAY)

    # Get data
    gaze = get_gaze_pts(os.path.join(ROOT_DIR,'text_files/gaze_pts_%s.csv' %device))
    frame_nb, ordered_pts = load_data(ROOT_DIR, device, method)
    # get warped image canvas
    dst, maxWidth, maxHeight = build_image(SCREEN_REF_COORDS)
    # initialize progress bar for command line
    k = len(ordered_pts)
    printProgressBar(0, k, prefix = 'Progress:', suffix = 'Complete', length = 50)

    for l,(p,i)in enumerate(zip(ordered_pts, frame_nb)):
        # sort points clockwise
        p = sortpts_clockwise(np.array(p,dtype=np.float32))
        # get frame
        cap.set(cv2.CAP_PROP_POS_FRAMES,i)
        ret, frame = cap.read()
        fc = frame.copy()
        # get gaze points
        gaze_pts = gaze[i-int(gaze[0]['frame'])]['pts']
        for pt in gaze_pts:
            gx = pt[0]
            gy = pt[1]
            cv2.circle(fc,(gx,gy),10,(0,255,150),-1)
            cv2.imshow('Gaze in real', fc)
            cv2.waitKey(1)
        # Get transformation matrix
        M = cv2.getPerspectiveTransform(np.array(p,dtype='float32'), dst)
        # Warp the image
        warp = cv2.warpPerspective(frame, M, (maxWidth, maxHeight))
        WARPED_SCREENS.append(warp)
        pts = [(int(p[0][0]), int(p[0][1]))
                ,(int(p[1][0]), int(p[1][1]))
                ,(int(p[2][0]), int(p[2][1]))
                ,(int(p[3][0]), int(p[3][1]))]
        # Get homography matrix in different form (same matrix different shape)
        H, __ = cv2.findHomography(np.array(pts), np.array(SCREEN_REF_COORDS), cv2.RANSAC,5.0)
        # invert matrix
        H_inv = np.linalg.inv(H)

        rc = gray_copy.copy()

        if showMore:
            y_offset = SCREEN_REF_COORDS[0][1]
            x_offset = SCREEN_REF_COORDS[0][0]
            dst_gray = cv2.cvtColor(warp, cv2.COLOR_RGB2GRAY)
            h,w = dst_gray.shape[:2]
            rc[y_offset:y_offset+h, x_offset:x_offset+w] = dst_gray
            cv2.imshow('Insert', rc)
            cv2.waitKey(1)

        pts_corner = np.float32(CORNER_REF_COORDS).reshape(-1,1,2)
        dst_corner = cv2.perspectiveTransform(pts_corner,H_inv)
        pts_screen = np.float32(SCREEN_REF_COORDS).reshape(-1,1,2)
        dst_screen = cv2.perspectiveTransform(pts_screen,H_inv)

        # get min and max coordinates of the mapped corner coordinates
        cornerX = []
        cornerY = []
        for pt in dst_corner:
            cornerX.append(int(pt[0][0]))
            cornerY.append(int(pt[0][1]))
        mincornerX = min(cornerX)
        mincornerY = min(cornerY)
        maxcornerX = max(cornerX)
        maxcornerY = max(cornerY)

        if showMore:
            for pt in dst_corner:
                cv2.circle(fc, (pt[0][0], pt[0][1]), 4, (0,0,255), -1)
                cv2.imshow('fc', fc)
                cv2.waitKey(1)

            for pt in dst_screen:
                cv2.circle(fc, (pt[0][0], pt[0][1]), 4, (0,255,255), -1)
                cv2.imshow('fc', fc)
                cv2.waitKey(1)

        for j,pt in enumerate(gaze_pts):
            gx = pt[0]
            gy = pt[1]
            pts_gaze_ref = None
            pts_gaze_scene = np.array([[gx, gy]], dtype='float32')
            pts_gaze_scene = np.array([pts_gaze_scene])
            # transform the gaze point to reference screen
            pts_gaze_ref = cv2.perspectiveTransform(pts_gaze_scene,H)
            if showMore:
                cv2.circle(rc,((int(pts_gaze_ref[0][0][0]),int(pts_gaze_ref[0][0][1]))),20,(0,200,255),5)
                cv2.imshow('Insert', rc)
                cv2.waitKey(1)
            # save result if gaze point if on device
            if mincornerX < gx < maxcornerX and mincornerY < gy < maxcornerY:
                if saveResults:
                    cv2.imwrite(os.path.join(ROOT_DIR,'Evaluation/warped/%s_%s/warped_screen_%s.png' %(device,method,i)), warp)

                    with open(os.path.join(ROOT_DIR,'text_files/eval_mapped_gaze_%s_%s.csv' %(device,method)), mode='a') as eval_file:
                        eval_writer = csv.writer(eval_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        if l == 0 and j == 0:
                            eval_writer.writerow(['frame', 'x', 'y'])

                        eval_writer.writerow(['%s' %i, '%s' %pts_gaze_ref[0][0][0], '%s' %pts_gaze_ref[0][0][1]])
        # Update Progress Bar
        printProgressBar(l + 1, k, prefix = 'Progress:', suffix = 'Complete', length = 50)