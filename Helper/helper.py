from scipy.spatial import distance as dist
import cv2
import os
import numpy as np

def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i:i+n]

def sortpts_clockwise(A,idx=None):
    if idx is None:
        # Sort A based on Y(col-2) coordinates
        sortedAc2 = A[np.argsort(A[:,1]),:]

        # Get top two and bottom two points
        top2 = sortedAc2[0:2,:]
        bottom2 = sortedAc2[2:,:]

        # Sort top2 points to have the first row as the top-left one
        sortedtop2c1 = top2[np.argsort(top2[:,0]),:]
        top_left = sortedtop2c1[0,:]

        # Use top left point as pivot & calculate sq-euclidean dist against
        # bottom2 points & thus get bottom-right, bottom-left sequentially
        sqdists = dist.cdist(top_left[None], bottom2, 'sqeuclidean')
        rest2 = bottom2[np.argsort(np.max(sqdists,0))[::-1],:]

        # Concatenate all these points for the final output
        return np.concatenate((sortedtop2c1,rest2),axis =0)
    elif idx == 0:
        # Sort A based on X(col-2) coordinates
        sortedAc1 = A[np.argsort(A[:,0]),:]
        # most left is bottom left point
        bl = sortedAc1[0]
        # others are on right side
        right=sortedAc1[1:]
        # sort by Y coordinate
        sortedAc2 = right[np.argsort(right[:,1]),:]
        tr = sortedAc2[0]
        br = sortedAc2[1]
        return np.array([tr,br,bl],dtype='int32')
    elif idx == 1:
        # Sort A based on X(col-2) coordinates
        sortedAc1 = A[np.argsort(A[:,0]),:]
        # most right is bottom right point
        br = sortedAc1[-1]
        # others are on right side
        left=sortedAc1[0:2]
        # sort by Y coordinate
        sortedAc2 = left[np.argsort(left[:,1]),:]
        tl = sortedAc2[0]
        bl = sortedAc2[1]
        return np.array([tl,br,bl],dtype='int32')
    elif idx == 2:
        # Sort A based on X(col-2) coordinates
        sortedAc1 = A[np.argsort(A[:,0]),:]
        # most right is bottom right point
        tr = sortedAc1[-1]
        # others are on right side
        left=sortedAc1[0:2]
        # sort by Y coordinate
        sortedAc2 = left[np.argsort(left[:,1]),:]
        tl = sortedAc2[0]
        bl = sortedAc2[1]
        return np.array([tl,tr,bl],dtype='int32')
    elif idx == 3:
        # Sort A based on X(col-2) coordinates
        sortedAc1 = A[np.argsort(A[:,0]),:]
        # most right is bottom right point
        tl = sortedAc1[0]
        # others are on right side
        right=sortedAc1[1:]
        # sort by Y coordinate
        sortedAc2 = right[np.argsort(right[:,1]),:]
        tr = sortedAc2[0]
        br = sortedAc2[1]
        return np.array([tl,tr,br],dtype='int32')

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def get_layout(device, ROOT_DIR):
    return cv2.imread(os.path.join(ROOT_DIR, 'assets/%s/layout/layout.png' %device))

def get_ratios(pts):
    h1 = dist.euclidean(pts[1], pts[2])
    h2 = dist.euclidean(pts[3], pts[0])
    w1 = dist.euclidean(pts[0], pts[1])
    w2 = dist.euclidean(pts[2], pts[3])

    return h1/w1, h2/w2
