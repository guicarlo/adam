"""
Screen matching

Ehrlich-Adám Constantin
pdz, ETH Zürich
2019

-------------------------------------------------
Usage

With no previously trained network
python3 screen_matching_parallel.py --device=device-name --method=method-name --saveResults=bool --cores=nb_Cores_to_use

"""

import cv2
import os
import numpy as np
import imutils
import pdb
from math import *
import glob
import csv
import scipy.signal as ss
from Helper.helper import printProgressBar
from time import time
import re
from multiprocessing import Pool
import pandas

ROOT_DIR = './'

def get_screen_names(device):
    return sorted([f for f in os.listdir(os.path.join(ROOT_DIR,'assets/%s/screens' %device)) if (f.endswith('.png') or f.endswith('.jpg'))])

def get_screens(screen_names,device):
    screens = []
    for name in screen_names:
        screens.append(cv2.imread(os.path.join(ROOT_DIR, 'assets/%s/screens/%s' %(device, name))))
    return screens

# def brisk(screens, img):

def brisk_screen_matching(dst):
    img = cv2.resize(dst[0],(int(dst[0].shape[1]/2),int(dst[0].shape[0]/2)))
    screen_names = get_screen_names(args.device)
    screens = get_screens(screen_names, args.device)
    resized_screens = []
    for s in screens:
        resized_screens.append(imutils.resize(s,width=img.shape[1], height=img.shape[0]))
    good_matches = []
    brisk = cv2.BRISK_create(10,3)
    kp1, des1 = brisk.detectAndCompute(img,None)
    for l,screen in enumerate(resized_screens):
        # For every screen in database do sift matching with image
        # find the keypoints and descriptors with SIFT
        kp0, des0 = brisk.detectAndCompute(screen,None)

        bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
        # Match descriptors.
        matches = bf.match(des0,des1)
        # Sort them in the order of their distance.
        matches = sorted(matches, key = lambda x:x.distance)
        good = []
        for m in matches:
            # Minimum distance keypoints must have
            if m.distance < 80:
                good.append(m)
        good_matches.append(len(good))
        # img3 = cv2.drawMatches(screen,kp0,img,kp1,good,None, flags=2)
        # cv2.imshow('matches', img3)
        # cv2.waitKey(1)
    # Get screen index with highest match number
    max_good_matches_idx = np.argmax(good_matches)
    with open(os.path.join(ROOT_DIR,'text_files/screen_matching_%s_%s.csv' %(args.device, args.method)), mode='a') as eval_file:
        eval_writer = csv.writer(eval_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        if l == 0:
            eval_writer.writerow(['frame', 'screen_index'])

        eval_writer.writerow(['%s' %dst[1], '%s' %max_good_matches_idx])


def get_warped_screens(filenames, device, method):
    path = os.path.join(ROOT_DIR,'Evaluation/warped/%s_%s' %(device, method))
    warped = []
    frames = []
    numbers = re.compile(r'(\d+)')
    for name in filenames:
        frames.append(int(numbers.split('%s' %name)[1]))
        warped.append(cv2.imread(os.path.join(ROOT_DIR,name),0))

    return warped, frames

def sort_filenames(device,method):
    import re
    numbers = re.compile(r'(\d+)')
    def numericalSort(value):
        parts = numbers.split(value)
        parts[1::2] = map(int, parts[1::2])
        return parts

    filenames = [img for img in glob.glob("Evaluation/warped/%s_%s/*.png" %(device, method))]
    filenames = sorted(glob.glob('Evaluation/warped/%s_%s/*.png' %(device, method)), key=numericalSort)
    return filenames

def filter_SIFT_index(index):
    # Median filter one and two
    filt1 = ss.medfilt(index,kernel_size=3)
    filt2 = ss.medfilt(filt1,kernel_size=7)
    return filt2

def get_SIFT_matches(device,method):
    df = pandas.read_csv('./text_files/screen_matching_%s_%s.csv' %(device, method), header=None, names=['frame', 'screen'],sep=',',low_memory=False)
    screen = []
    pair = []
    for f,s in zip(df['frame'],df['screen']):
        pair.append((int(f), int(s)))
    sorted_by_first = sorted(pair, key=lambda tup: tup[0])
    sorted_screen = []
    sorted_frame = []
    for p in sorted_by_first:
        sorted_screen.append(p[1])
        sorted_frame.append(p[0])
    return sorted_screen, sorted_frame

if __name__ == '__main__':
    import argparse

    print('Screen matching')

    parser = argparse.ArgumentParser(
        description='Screen matching')
    parser.add_argument('--device', required=True,
                        metavar="device",
                        help='Device Name')
    parser.add_argument('--method', required=True,
                        metavar="method",
                        help='SLSD/FLSD')
    parser.add_argument("--saveResults",
                        metavar="<saveResults>",
                        help="save results in a file")
    parser.add_argument("--cores",
                        metavar="<cores>",
                        help="Number of cores")
    args = parser.parse_args()
    filenames = sort_filenames(args.device, args.method)
    warped, frames = get_warped_screens(filenames, args.device, args.method)
    resized_screens = []

    # warped = warped[0:100]
    # frames = frames[0:100]
    parallel = []
    for w, f in zip(warped, frames):
        parallel.append((w,f))
    nb_cores = int(args.cores)
    pool = Pool(nb_cores)
    pool.map(brisk_screen_matching, parallel)
    sorted_screen, sorted_frame = get_SIFT_matches(args.device, args.method)
    filtered = filter_SIFT_index(sorted_screen)
    if args.saveResults == 'True':
        for l, (idx, fnb) in enumerate(zip(filtered,sorted_frame)):
            with open(os.path.join(ROOT_DIR,'text_files/filtered_screen_matching_%s_%s.csv' %(args.device, args.method)), mode='a') as eval_file:
                eval_writer = csv.writer(eval_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                if l == 0:
                    eval_writer.writerow(['frame', 'filtered_screen_index'])

                eval_writer.writerow(['%s' %fnb, '%s' %int(idx)])
    # t1 = time()
    # total = t1-t0
    # print(total)



