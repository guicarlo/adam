"""
Preparation work for data analysis

Ehrlich-Adám Constantin
pdz, ETH Zürich
2019

-------------------------------------------------
Usage

python3 prep.py
"""

import os
import cv2
import sys
import json

# for evaluation
from time import time
import csv

from Helper.helper import get_layout

ROOT_DIR = './'

DEVICE_NAME = ''

SCREEN_REF_COORDS = []
CORNER_REF_COORDS = []
TMP = []
IS_SCREEN_REF = bool
JSON_DATA = []

def get_platform():
    # Get platform for platform depending command line commands
    platform = sys.platform
    print_platform = ''
    if platform == 'darwin':
        print_platform = 'MacOS'
    elif platform == 'win32':
        print_platform = 'Windows'
    elif platform == 'linux2':
        print_platform = 'Linux'
    else:
        print_platform = 'Unknown'
    print('Running on', print_platform)

    return platform

def get_video_file(platform):
    # Get the video file copy it to folder and rename it
    print('Please enter the path to your video file (absolute Path)')
    video_file_path = input()
    video_name = video_file_path.split('/')[-1]
    if platform == 'darwin':
        print('Copying Video')
        os.system('cp %s ./assets/Videos/' %video_file_path)
        print('Renaming Video to video.avi')
        os.system('mv  ./assets/Videos/%s ./assets/Videos/video_%s.avi' %(video_name, DEVICE_NAME))
    else:
        print('Copying Video')
        os.system('copy %s ./assets/Videos/' %video_file_path)
        print('Renaming Video to video.avi')
        os.system('move  ./assets/Videos/%s ./assets/Videos/video_%s.avi' %(video_name, DEVICE_NAME))
    print('Video file copied to ./assets/Videos/')

def get_raw_data_file(platform):
    # Get the raw data file copy it to folder and rename it
    print('Please enter the path to your raw data (absolute Path)')
    raw_data_file_path = input()
    data_name = raw_data_file_path.split('/')[-1]
    if platform == 'darwin':
        print('Copying File')
        os.system('cp %s ./assets/Raw_Data/' %raw_data_file_path)
        print('Renaming File to raw_data_%s.csv')
        os.system('mv  ./assets/Raw_Data/%s ./assets/Raw_Data/raw_data_%s.csv' %(data_name, DEVICE_NAME))
    else:
        print('Copying File')
        os.system('copy %s ./assets/Raw_Data/' %raw_data_file_path)
        print('Renaming File to raw_data_%s.csv')
        os.system('move  ./assets/Raw_Data/%s ./assets/Videos/raw_data_%s.csv' %(data_name, DEVICE_NAME))
    print('raw data copied ./assets/Raw_Data/')

def create_device_folder():
    # Create a device folder structure
    global DEVICE_NAME

    print('What is the name of the device you want to analyse?')
    DEVICE_NAME = input()
    print('Create folder ./assets/%s' %DEVICE_NAME)
    os.mkdir(os.path.join(ROOT_DIR,'assets/%s' %DEVICE_NAME))

def get_layout_file(platform):
    # Get the layout file copy it to folder and rename it
    print('Create folder assets/%s/layout' %DEVICE_NAME)
    os.mkdir(os.path.join(ROOT_DIR,'assets/%s/layout' %DEVICE_NAME))

    print('Please enter the path to your device layout file')
    layout_file_path = input()
    if platform == 'darwin':
        os.system('cp %s assets/%s/layout' %(layout_file_path,DEVICE_NAME))
    else:
        os.system('copy %s assets/%s/layout' %(layout_file_path,DEVICE_NAME))
    return layout_file_path

def get_screens_files(platform):
    # Get the screen files copy it to folder and rename it
    print('Please enter the path to the folder of the screen layout files')
    screen_file_path = input()
    folder_name = screen_file_path.split('/')[-1]
    if platform == 'darwin':
        os.system('cp -r %s assets/%s/' %(screen_file_path,DEVICE_NAME))
        os.system('mv assets/%s/%s assets/%s/screens' %(DEVICE_NAME,folder_name, DEVICE_NAME))
    else:
        os.system('copy -r %s assets/%s/' %(screen_file_path,DEVICE_NAME))
        os.system('move assets/%s/%s assets/%s/screens' %(DEVICE_NAME,folder_name, DEVICE_NAME))

def click(event, x, y, flags, param):
    # grab references to the global variables
    global SCREEN_REF_COORDS, CORNER_REF_COORDS, IS_SCREEN_REF, TMP
    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        TMP.append((x,y))
        if IS_SCREEN_REF:
            cv2.circle(layout, (x,y), 10, (255, 0, 0), -1)
            cv2.imshow('GET REF COORDS', layout)
        else:
            cv2.circle(layout, (x,y), 10, (0, 0, 255), -1)
            cv2.imshow('GET REF COORDS', layout)

        if len(TMP) == 1:
            print('Top-Left: ', TMP[0])
        if len(TMP) == 2:
            print('Top-Right: ', TMP[1])
        if len(TMP) == 3:
            print('Bottom-Right: ', TMP[2])
        if len(TMP) == 4:
            print('Bottom-Left: ', TMP[3])
            print('Reference Coords:', TMP)

def get_ref_coords(layout):
    global TMP, SCREEN_REF_COORDS, CORNER_REF_COORDS, IS_SCREEN_REF
    TMP = []
    h,w = layout.shape[:2]
    cv2.namedWindow('GET REF COORDS')
    cv2.setMouseCallback('GET REF COORDS', click)
    cv2.putText(layout,'SCREEN_REF_COORDS',(50,50),cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,0))
    cv2.putText(layout,'CORNER_REF_COORDS',(w-500,50),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255))
    lc = layout.copy()

    # keep looping until the 'q' key is pressed
    while len(TMP) != 4:
        # display the image and wait for a keypress
        cv2.imshow('GET REF COORDS', layout)
        key = cv2.waitKey(1) & 0xFF
        # if the 'r' key is pressed, reset the cropping region
        if key == ord("r"):
            layout = lc.copy()
            print('Reset')
            TMP = []
            if IS_SCREEN_REF:
                SCREEN_REF_COORDS = []
                print('Screen Reference: ', SCREEN_REF_COORDS)
            else:
                CORNER_REF_COORDS = []
                print('Corner Reference: ', CORNER_REF_COORDS)

        # if the 'c' key is pressed, break from the loop
        elif (key == ord("c")):
            break

    if IS_SCREEN_REF:
        SCREEN_REF_COORDS = TMP[:]
    else:
        CORNER_REF_COORDS = TMP[:]
    print('SCREEN:', SCREEN_REF_COORDS)
    print('CORNER:', CORNER_REF_COORDS)

if __name__ == '__main__':
    print('Preparation Work for data analysis')
    platform = get_platform()

    create_device_folder()
    get_video_file(platform)
    get_raw_data_file(platform)
    get_layout_file(platform)
    get_screens_files(platform)

    layout = get_layout(DEVICE_NAME, ROOT_DIR)
    # Get Screen coords
    print('PLEASE SELECT THE SCREEN CORNERS IN CLOCKWISE DIRECTION')
    IS_SCREEN_REF = True
    get_ref_coords(layout)
    # Get Corner coords
    print('PLEASE SELECT THE DEVICE CORNERS IN CLOCKWISE DIRECTION')
    IS_SCREEN_REF = False
    get_ref_coords(layout)

    # Save screen and corner coords
    with open(os.path.join(ROOT_DIR,'assets/%s/layout/corner_coords.json' %DEVICE_NAME), 'w') as json_file:
        data = {
            'name': 'screen',
            'points': SCREEN_REF_COORDS
        }
        data2 = {
            'name': 'corner',
            'points': CORNER_REF_COORDS
        }
        JSON_DATA.append(data)
        JSON_DATA.append(data2)
        json.dump(JSON_DATA, json_file)

    print('All set')

    print('All set')


