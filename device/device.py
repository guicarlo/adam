"""
Mask R-CNN
Train your device dataset and show mask effect.

Copyright (c) 2018 Matterport, Inc.
Licensed under the MIT License (see LICENSE for details)
Written by Waleed Abdulla

Adapted by Constantin Ehrlich-Adám
pdz, ETH Zürich
2019


------------------------------------------------------------

Usage: import the module (see Jupyter notebooks for examples), or run from
       the command line as such:

    # Train a new model starting from pre-trained COCO weights
    python3 device.py train --device=device-name --dataset=/path/to/device/dataset --weights=coco
    --video=name/of/video/in/assets/Videos/folder --file=name/of/file/with/tracked/pts

    # Resume training a model that you had trained earlier
    python3 device.py train --device=device-name --dataset=/path/to/device/dataset --weights=last
    --video=name/of/video/in/assets/Videos/folder --file=name/of/file/with/tracked/pts
"""



import os
import sys
import json
import datetime
import numpy as np
import pdb
import cv2
import skimage.draw
import random

# Root directory of the project
ROOT_DIR = os.path.abspath("../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

# Path to trained weights file
COCO_WEIGHTS_PATH = os.path.join(ROOT_DIR, "weights/mask_rcnn_coco.h5")
WANDA_WEIGHTS_PATH = os.path.join(ROOT_DIR, "weights/mask_rcnn_wanda.h5")
HAMILTON_WEIGHTS_PATH = os.path.join(ROOT_DIR, "weights/mask_rcnn_hamilton.h5")

# Directory to save logs and model checkpoints, if not provided
# through the command line argument --logs
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")

############################################################
#  Configurations
############################################################

class DeviceConfig(Config):
    """Configuration for training on the toy  dataset.
    Derives from the base Config class and overrides some values.
    """
    # Give the configuration a recognizable name
    NAME = "device"

    # We use a GPU with 12GB memory, which can fit two images.
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 2

    # Number of classes (including background)
    NUM_CLASSES = 1 + 1  # Background + device

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 100

    # Skip detections with < 90% confidence
    DETECTION_MIN_CONFIDENCE = 0.9

############################################################
#  Dataset
############################################################

class DeviceDataset(utils.Dataset):

    def load_device(self, dataset_dir, subset):
        """Load a subset of the Device dataset.
        dataset_dir: Root directory of the dataset.
        subset: Subset to load: train or val
        """
        # Add classes. We have only one class to add.
        self.add_class("device", 1, "device")

        # Train or validation dataset?
        assert subset in ["train", "val"]
        dataset_dir = os.path.join(dataset_dir, subset)

        # Load annotations
        # {"frame_nb": 28520,
        # "shape_attributes": {
        #    "name": "polygon",
        #    "all_points_x": [582, 719, 720, 592],
        #    "all_points_y": [582, 719, 720, 592]
        # }
        # }
        # We mostly care about the x and y coordinates of each region
        annotations = json.load(open(os.path.join(dataset_dir, "tracked_pts.json")))

        # Skip unannotated images.
        annotations = [a for a in annotations if a['shape_attributes']]

        # Add images
        for a in annotations:
            # Get the x, y coordinaets of points of the polygons that make up
            # the outline of each object instance. These are stores in the
            # shape_attributes (see json format above)
            polygons = [a['shape_attributes']]
            height = a['height']
            width = a['width']
            self.add_image(
                "device",
                image_id=a['frame_nb'],  # use file name as a unique image id
                width=width, height=height,
                polygons=polygons)

    def load_mask(self, image_id):
        """Generate instance masks for an image.
       Returns:
        masks: A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids: a 1D array of class IDs of the instance masks.
        """
        # If not a device dataset image, delegate to parent class.
        image_info = self.image_info[image_id]
        if image_info["source"] != "device":
            return super(self.__class__, self).load_mask(image_id)

        # Convert polygons to a bitmap mask of shape
        # [height, width, instance_count]
        info = self.image_info[image_id]
        mask = np.zeros([info["height"], info["width"], len(info["polygons"])],
                        dtype=np.uint8)
        for i, p in enumerate(info["polygons"]):
            # Get indexes of pixels inside the polygon and set them to 1
            rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            mask[rr, cc, i] = 1

        # Return mask, and array of class IDs of each instance. Since we have
        # one class ID only, we return an array of 1s
        return mask.astype(np.bool), np.ones([mask.shape[-1]], dtype=np.int32)

    def image_reference(self, image_id):
        """Return the path of the image."""
        info = self.image_info[image_id]
        if info["source"] == "device":
            return info["path"]
        else:
            super(self.__class__, self).image_reference(image_id)

def create_dataset(file_name, size):
    tracked_pts = json.load(open(os.path.join(ROOT_DIR, "text_files/%s" %file_name)))
    nb_pts = len(tracked_pts)
    val_size = int(0.1*size/0.9)
    size_train_set = [size]
    size_val_set = [val_size]
    half = int(nb_pts/2)
    for st, sv in zip(size_train_set, size_val_set):
        sub_train = []
        sub_val = []

        for i in range(0,st,1):
            sub_train.append(tracked_pts[random.randint(0,half)])
        for i in range(0,sv,1):
            sub_val.append(tracked_pts[random.randint(half,nb_pts)])

        train_data = []
        val_data = []
        for item in range(0,len(sub_train)-1, 1):
            with open('dataset/train/tracked_pts_%s.json' %st, 'w') as json_file:
                train_data.append(sub_train[item])
                json.dump(train_data, json_file)

        for item in range(0, len(sub_val)-1, 1):
            with open('dataset/val/tracked_pts_%s.json' %st, 'w') as json_file:
                val_data.append(sub_val[item])
                json.dump(val_data, json_file)

def train(model):
    """Train the model."""
    video_file_path = os.path.join(ROOT_DIR, 'assets/Videos/%s' %args.video)

    # Training dataset.
    dataset_train = DeviceDataset()
    dataset_train.add_video_reference(video_file_path)
    dataset_train.load_device(args.dataset, "train")
    dataset_train.prepare()

    # Validation dataset
    dataset_val = DeviceDataset()
    dataset_val.add_video_reference(video_file_path)
    dataset_val.load_device(args.dataset, "val")
    dataset_val.prepare()

    # *** This training schedule is an example. Update to your needs ***
    # Since we're using a very small dataset, and starting from
    # COCO trained weights, we don't need to train too long. Also,
    # no need to train all layers, just the heads should do it.
        # Augmentation
    # augmentation = aug.SomeOf((0, 2), [
    #     aug.Fliplr(0.5),
    #     aug.Flipud(0.5),
    #     aug.OneOf([aug.Affine(rotate=90), aug.Affine(rotate=180), aug.Affine(rotate=270)]),
    #     aug.Affine(translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)})
    # ])
    # model.train(dataset_train, dataset_val,
    #             learning_rate=config.LEARNING_RATE,
    #             epochs=15,
    #             layers='heads',
    #             augmentation=augmentation)


    print("Training network heads")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=15,
                layers='heads')



############################################################
#  Training
############################################################

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN to detect device.')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'train' or 'create'")
    parser.add_argument('--device', required=True,
                        metavar="device",
                        help='Device name')
    parser.add_argument('--dataset', required=True,
                        metavar="/path/to/device/dataset/",
                        help='Directory of the device dataset')
    parser.add_argument('--weights', required=True,
                        metavar="/path/to/weights.h5",
                        help="Path to weights .h5 file or 'coco'")
    parser.add_argument('--file', required=False,
                        metavar="/path/to/file.json",
                        help="Path to file.json")
    parser.add_argument('--video', required=True,
                        metavar="path or URL to video",
                        help='Video used for tracked points')
    parser.add_argument('--logs', required=False,
                        default=DEFAULT_LOGS_DIR,
                        metavar="/path/to/logs/",
                        help='Logs and checkpoints directory (default=logs/)')
    parser.add_argument('--size', required=False,
                        metavar="size of dataset",
                        help='Size of training dataset')

    args = parser.parse_args()
    file_name = args.file

    if args.command == "create":
        # Create correct dataset setup
        train_size = int(args.size)
        create_dataset(args.file, train_size)

    if args.command == "train":
        # Validate arguments
        assert args.dataset, "Argument --dataset is required for training"
        # Configurations
        config = DeviceConfig()
        config.NAME = args.device
        config.display()
        # Create model
        model = modellib.MaskRCNN(mode="training", config=config,
                                  model_dir=args.logs)
    else:
        print('ERROR - Please check your input')

    print("Weights: ", args.weights)
    print("Dataset: ", args.dataset)
    print("Logs: ", args.logs)


    # Select weights file to load
    if args.weights.lower() == "coco":
        weights_path = COCO_WEIGHTS_PATH
        # Download weights file
        if not os.path.exists(weights_path):
            utils.download_trained_weights(weights_path)
    elif args.weights.lower() == "last":
        # Find last trained weights
        weights_path = model.find_last()
    # elif args.weights.lower() == 'wanda':
    #     weights_path = WANDA_WEIGHTS_PATH
    # elif args.weights.lower() == 'hamilton':
    #     weights_path = HAMILTON_WEIGHTS_PATH
    elif args.weights.lower() == args.device:
        weights_path = os.path.join(ROOT_DIR, "weights/mask_rcnn_%s.h5" %args.device)
    else:
        weights_path = args.weights

    # Load weights
    print("Loading weights ", weights_path)
    if args.weights.lower() == "coco":
        # Exclude the last layers because they require a matching
        # number of classes
        model.load_weights(weights_path, by_name=True, exclude=[
            "mrcnn_class_logits", "mrcnn_bbox_fc",
            "mrcnn_bbox", "mrcnn_mask"])
    else:
        model.load_weights(weights_path, by_name=True)

    # Train or evaluate
    if args.command == "train":
        train(model)
    else:
        print("'{}' is not recognized. "
              "Use 'train'".format(args.command))
